import { config } from '../config';
class Debug {
	name: String;
	constructor(name: String) {
		this.name = name;
	}

	/**
	 *
	 * @param {String} str Message
	 * @param {String} fname Function name
	 * @param {Object} data Data to print out
	 * @param {Number} method can be -1 0 1 as error warn and info
	 */
	print(str: String, fname: String, data: Object = null, method: Number = 1) {
		switch (method) {
			case -1: {
				this.error(
					this.name.toUpperCase() + ' <> ' + fname + ' <> ' + str,
					data,
				);
				break;
			}
			case 0: {
				if (config.DEBUG.LEVEL > 0)
					this.warn(
						this.name.toUpperCase() + ' <> ' + fname + ' <> ' + str,
						data,
					);
				break;
			}
			default: {
				if (config.DEBUG.LEVEL > 1)
					this.log(
						this.name.toUpperCase() + ' <> ' + fname + ' <> ' + str,
						data,
					);
				break;
			}
		}
	}

	/**
	 * Log an info to console
	 * @param {String} str String to be print
	 * @param {Object} data Data to log out
	 */
	log(str: String, data: Object = null) {
		if (!config.DEBUG.ACTIVE) return;
		if (data) {
			console.log(str, '<> ' + typeof data);
			console.log(data);
		} else {
			console.log(str);
		}
	}

	/**
	 * Warn an info to console
	 * @param {String} str String to be print
	 * @param {Object} data Data to log out
	 */
	warn(str: String) {
		if (!config.DEBUG.ACTIVE) return;
		console.warn(str);
	}

	/**
	 * Log an error to console
	 * @param {String} str String to be print
	 * @param {Object} data Data to log out
	 */
	error(str: String, data: Object = null) {
		if (!config.DEBUG.ACTIVE) return;
		if (data) {
			console.error(str, '<> ' + typeof data);
			console.error(data);
		} else {
			console.error(str);
		}
	}
}

export { Debug };
