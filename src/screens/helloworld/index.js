import React from 'react';
import './index.scss';
import { Link } from 'react-router-dom';
function HelloWorld() {
	return (
		<div className="screen">
			<Link to="/hello" style={{ width: '300px' }} id="text">
				<img
					src={require('../../assets/images/logo.svg')}
					className="animated rotateIn"
				/>
			</Link>
			<h2 className="animated delay-1s zoomIn">Present by saintno</h2>
		</div>
	);
}

export { HelloWorld };
