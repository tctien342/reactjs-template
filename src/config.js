const config = {
	/**
	 * Config debug of app
	 */
	DEBUG: {
		ACTIVE: true,
		LEVEL: 3,
	},
};
export { config };
